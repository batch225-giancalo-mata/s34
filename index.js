
const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// 1. Create a GET route that will access the "/home" route that will print out a simple message.
// 2. Process a GET request at the "/home" route using postman.


app.get("/home", (req, res) => {
	res.send("Welcome to the home page");
});


// 3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
// 4. Process a GET request at the "/users" route using postman.

let users = [];

app.post("/add-users", (req, res) => {

	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){

		users.push(req.body);

		res.send(`User ${req.body.username} successfully registered`);
	} else {

		res.send("Please input BOTH username and password.")
	}

}); 


// 5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
// 6. Process a DELETE request at the "/delete-user" route using postman.

app.delete("/delete-user", (req, res) => {

	// Creates a variable to store the message to be sent back to the Client/Postman.

	let message;

	// Create a "for loop" that will loop through the elements of the users array.

	for(let i = 0; i < users.length; i++){

		// If the username provided in the Client/Postman and the username of the current object in the loop is the same.

		if (req.body.username == users[i].username){

			users.pop(req.body);

			message = `User ${req.body.username} has been deleted.`;

			break;

		} else {

			message = "User not found.";
		}

	}

	res.send(message);

});

app.get('/users-array', (req, res) => {
	res.json(users);
});

app.listen(port, () => console.log(`Server running at port ${port}`))

// 7. Export the Postman collection and save it inside the root folder of our application.
// 8. Create a git repository named S34.
// 9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 10. Add the link in Boodle.


